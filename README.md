# Arlanda Planner

Arlanda Planner is an android app that takes away the burden of planning your journey to Arlanda Airport.

It finds your flight by destination city and calculates the best route to Arlanda airport via Stockholm public transport and Flygbuss.

## How does it work?
1. Fill in your home adress and time preferences for arrival
2. Enter your destination city
3. Receive your SL and Flygbuss route options
4. Enjoy your worry-free commute

## What makes it work?
* Api from
  * SL
  * Flygbuss
  * SwedAvia

## Demo video
![](arlandaplanner.mp4)