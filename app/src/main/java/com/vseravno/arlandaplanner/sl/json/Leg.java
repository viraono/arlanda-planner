package com.vseravno.arlandaplanner.sl.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Origin", "Destination", "name", "category", "type", "direction", "dist"})
public class Leg {

  @JsonProperty("Origin")
  private Origin origin;

  @JsonProperty("Destination")
  private Destination destination;

  @JsonProperty("name")
  private String name;

  @JsonProperty("category")
  private String category;

  @JsonProperty("type")
  private String type;

  @JsonProperty("direction")
  private String direction;

  @JsonProperty("dist")
  private String distance;

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getDistance() {
    return distance;
  }

  public void setDistance(String distance) {
    this.distance = distance;
  }

  @JsonProperty("Origin")
  public Origin getOrigin() {
    return origin;
  }

  @JsonProperty("Origin")
  public void setOrigin(Origin origin) {
    this.origin = origin;
  }

  @JsonProperty("Destination")
  public Destination getDestination() {
    return destination;
  }

  @JsonProperty("Destination")
  public void setDestination(Destination destination) {
    this.destination = destination;
  }

  @JsonProperty("name")
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("category")
  public String getCategory() {
    return category;
  }

  @JsonProperty("category")
  public void setCategory(String category) {
    this.category = category;
  }

  @JsonProperty("type")
  public String getType() {
    return type;
  }

  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }
}
