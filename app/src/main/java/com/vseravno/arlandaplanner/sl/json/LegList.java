package com.vseravno.arlandaplanner.sl.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Leg"})
public class LegList {

  @JsonProperty("Leg")
  private List<Leg> leg = null;

  @JsonProperty("Leg")
  public List<Leg> getLeg() {
    return leg;
  }

  @JsonProperty("Leg")
  public void setLeg(List<Leg> leg) {
    this.leg = leg;
  }
}
