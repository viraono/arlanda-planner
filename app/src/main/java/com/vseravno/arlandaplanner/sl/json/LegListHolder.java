package com.vseravno.arlandaplanner.sl.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"LegList"})
public class LegListHolder {

  @JsonProperty("LegList")
  private LegList legList;

  @JsonProperty("LegList")
  public LegList getLegList() {
    return legList;
  }

  @JsonProperty("LegList")
  public void setLegList(LegList legList) {
    this.legList = legList;
  }
}
