package com.vseravno.arlandaplanner.sl.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Trip"})
public class Response {

  @JsonProperty("Trip")
  private List<LegListHolder> tripsList = Collections.emptyList();

  @JsonProperty("Trip")
  public List<LegListHolder> getTripsList() {
    return tripsList;
  }

  @JsonProperty("Trip")
  public void setTripsList(List<LegListHolder> tripsList) {
    this.tripsList = tripsList;
  }
}
