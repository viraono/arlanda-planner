package com.vseravno.arlandaplanner.sl.client;

public class SlClientException extends Exception {
  public SlClientException(String message) {
    super(message);
  }

  public SlClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
