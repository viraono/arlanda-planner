package com.vseravno.arlandaplanner.sl.client;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vseravno.arlandaplanner.BuildConfig;
import com.vseravno.arlandaplanner.model.Station;
import com.vseravno.arlandaplanner.model.trip.Transport;
import com.vseravno.arlandaplanner.model.trip.Trip;
import com.vseravno.arlandaplanner.model.trip.TripPart;
import com.vseravno.arlandaplanner.model.trip.Type;
import com.vseravno.arlandaplanner.sl.json.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SlClient {
  private static final String tripKey = BuildConfig.slTripPlannerKey;
  private static final String stationKey = BuildConfig.slStationsKey;
  private static final String CENTRAL_STATION_ID = "1002";
  private static final int MIN_WALK = 50;
  private static final DateTimeFormatter HOUR_FORMAT = DateTimeFormatter.ofPattern("HH");
  private static final DateTimeFormatter MINUTE_FORMAT = DateTimeFormatter.ofPattern("mm");
  private String tripUrl = "https://api.sl.se/api2/TravelplannerV3_1/trip.json?key=";
  private String stationUrl = "http://api.sl.se/api2/typeahead.json?key=";

  @VisibleForTesting
  public SlClient(String tripUrl, String stationUrl) {
    this.tripUrl = tripUrl;
    this.stationUrl = stationUrl;
  }

  public SlClient() {}

  @RequiresApi(api = Build.VERSION_CODES.O)
  public List<Trip> getAllDepartures(LocalTime slDeadline, LocalDate date, int stationId)
      throws SlClientException {
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();
    Request request =
        new Request.Builder()
            .url(
                tripUrl
                    + tripKey
                    + "&originId="
                    + stationId
                    + "&destId="
                    + CENTRAL_STATION_ID
                    + "&searchForArrival=1&date="
                    + date.toString()
                    + "&time="
                    + slDeadline.format(HOUR_FORMAT)
                    + ":"
                    + slDeadline.format(MINUTE_FORMAT))
            .build();
    try (okhttp3.Response response = client.newCall(request).execute()) {
      if (response.code() != 200) {
        throw new SlClientException("Sl travel planner api returned code: " + response.code());
      }
      Response trips = mapper.readValue(response.body().string(), Response.class);
      return parseJson(trips);
    } catch (JsonParseException e) {
      throw new SlClientException("Failed to parse Sl travel planner api response", e);
    } catch (IOException | NullPointerException e) {
      throw new SlClientException("Failed to get Sl travel planner api request", e);
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.N)
  public List<Station> getStations(String stationName) throws SlClientException {
    String station = stationName.toLowerCase();
    JsonNode allStations = getAllStations(station, stationKey);
    return StreamSupport.stream(allStations.spliterator(), false)
        .filter(stn -> stn.get("Name").asText().toLowerCase().contains(station))
        .map(stn -> new Station(stn.get("Name").asText(), stn.get("SiteId").asInt()))
        .collect(Collectors.toList());
  }

  private JsonNode getAllStations(String stationName, String key) throws SlClientException {
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();
    JsonNode stations;
    Request request =
        new Request.Builder()
            .url(stationUrl + key + "&searchstring=" + stationName + "&stationsonly=true")
            .build();
    try (okhttp3.Response response = client.newCall(request).execute()) {
      if (response.code() != 200) {
        throw new SlClientException("Sl stations api returned code: " + response.code());
      }
      return mapper.readTree(response.body().string()).get("ResponseData");
    } catch (JsonParseException e) {
      throw new SlClientException("Failed to parse Sl stations api response", e);
    } catch (IOException | NullPointerException e) {
      throw new SlClientException("Failed to get Sl stations api request", e);
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private List<Trip> parseJson(Response json) {
    List<Trip> trips = new ArrayList<>();
    List<LegListHolder> jsonTrips = json.getTripsList();
    for (LegListHolder jsonTrip : jsonTrips) {
      Trip trip = new Trip();
      List<TripPart> tripParts = new ArrayList<>();
      List<Leg> legList = jsonTrip.getLegList().getLeg();
      for (Leg leg : legList) {
        legToTrip(leg).ifPresent(tripParts::add);
      }
      trip.setTrip(tripParts);
      trips.add(trip);
    }
    return trips;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private Optional<TripPart> legToTrip(Leg leg) {
    String originName = leg.getOrigin().getName();
    LocalTime originTime = LocalTime.parse(leg.getOrigin().getTime());
    String destName = leg.getDestination().getName();
    LocalTime destTime = LocalTime.parse(leg.getDestination().getTime());
    Type tripType = typeToEnum(leg.getType());
    if (tripType.equals(Type.WALK)) {
      int distance = Integer.parseInt(leg.getDistance());
      // skip trip part if walking distance is less than 50 meters
      if (distance >= MIN_WALK) {
        return Optional.of(
            new TripPart(originName, originTime, destName, destTime, tripType, distance));
      } else {
        return Optional.empty();
      }
    }
    String name = leg.getName();
    Transport transportType = transportToEnum(leg.getCategory());
    String direction = leg.getDirection();
    return Optional.of(
        new TripPart(
            originName, originTime, destName, destTime, name, transportType, tripType, direction));
  }

  private Type typeToEnum(String type) {
    switch (type) {
      case "JNY":
        return Type.RIDE;
      case "WALK":
        return Type.WALK;
    }
    return null;
  }

  private Transport transportToEnum(String category) {
    switch (category) {
      case "BUS":
        return Transport.BUS;
      case "TRN":
        return Transport.TRAIN;
      case "TRM":
        return Transport.TRAM;
      case "MET":
        return Transport.SUBWAY;
    }
    return null;
  }
}
