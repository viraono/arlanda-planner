package com.vseravno.arlandaplanner;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.vseravno.arlandaplanner.appview.PreferencesManager;
import com.vseravno.arlandaplanner.busclient.BusClient;
import com.vseravno.arlandaplanner.busclient.BusClientException;
import com.vseravno.arlandaplanner.model.Flygbuss;
import com.vseravno.arlandaplanner.model.Station;
import com.vseravno.arlandaplanner.model.trip.Trip;
import com.vseravno.arlandaplanner.model.trip.TripPart;
import com.vseravno.arlandaplanner.sl.client.SlClient;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.sl.client.SlClientException;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static java.time.Duration.ofMinutes;

@RequiresApi(api = Build.VERSION_CODES.O)
public class Manager {
  SlClient slClient = new SlClient();
  BusClient busClient = new BusClient();
  private final Duration WALK_TO_FLYGBUSS = ofMinutes(15);
  private Duration walkToStation;
  private Duration timeWithBaggage;
  private Duration timeNoBaggage;
  private int stationId;
  private List<Trip> slTrips;
  private Trip currentSlTrip;
  private LocalTime flygbussDeparture;
  private LocalTime slDeadLine;
  private List<Flygbuss> flygbussTrips;

  public Manager() {}

  public Manager(Context context) {
    setWalkToStation(PreferencesManager.getWalkToStation(context));
    setTimeWithBaggage(PreferencesManager.getWithBaggageTime(context));
    setTimeNoBaggage(PreferencesManager.getNoBaggageTime(context));
    setStationId(PreferencesManager.getStationId(context));
  }

  @VisibleForTesting
  public Manager(SlClient slClient, BusClient busClient) {
    this.slClient = slClient;
    this.busClient = busClient;
  }

  public Station[] getStations(String stationName) throws SlClientException {
    List<Station> stations = slClient.getStations(stationName);
    Station[] stationsArray = new Station[stations.size()];
    return stations.toArray(stationsArray);
  }

  public String[] calculateTrip(Flight flight) throws BusClientException, SlClientException {
    prepareData(flight);
    return collectAllInfo(getBestSlTripInfo());
  }

  public String[] showOneMoreSlTrip() {
    return getPreviousSlTrip().map(this::collectAllInfo).orElse(new String[] {"No more options!"});
  }

  private String[] collectAllInfo(List<String> slTrip) {
    String flygbuss = "Flygbuss leaves at " + flygbussDeparture + ".";
    String homeDeparture =
        "You need to leave at " + getSlDepartureTime().minus(walkToStation) + ".";
    slTrip.add("");
    slTrip.add(flygbuss);
    slTrip.add("");
    slTrip.add(homeDeparture);
    slTrip.add("____________________");
    return slTrip.toArray(new String[0]);
  }

  private void prepareData(Flight flight) throws BusClientException, SlClientException {
    LocalDate flightDate = flight.getDepartureDate();
    Duration inAirportTime;

    if (flight.isWithBaggage()) {
      inAirportTime = timeWithBaggage;
    } else {
      inAirportTime = timeNoBaggage;
    }

    LocalTime busArrivalDeadline = flight.getDepartureTime().minus(inAirportTime);
    if (flygbussTrips == null) {
      List<Flygbuss> flygBussTrips = busClient.getBusTrips(flightDate, busArrivalDeadline);
      if (flygBussTrips.isEmpty()) {
        throw new BusClientException("Failed to get FlygBuss api response");
      }
      this.setFlygbussTrips(flygBussTrips);
    }

    LocalTime busDeparture = findBestFlygbuss(busArrivalDeadline).getDeparture();
    this.setFlygbussDeparture(busDeparture);
    LocalTime slArrivalDeadline = busDeparture.minus(WALK_TO_FLYGBUSS);
    this.setSlDeadLine(slArrivalDeadline);

    if (slTrips == null) {
      List<Trip> allSlDepartures =
          slClient.getAllDepartures(slArrivalDeadline, flightDate, stationId);
      if (allSlDepartures.isEmpty()) {
        throw new SlClientException("Failed to get Sl travel planner api response");
      }
      this.setSlTrips(allSlDepartures);
    }
  }

  private Flygbuss findBestFlygbuss(LocalTime arrivalDeadline) {
    Flygbuss bestFlygbuss = flygbussTrips.stream().filter(Flygbuss::isBestMatch).findFirst().get();
    LocalTime flygbussArrival = bestFlygbuss.getArrival();

    // extra check for bus arrival
    if (flygbussArrival.isAfter(arrivalDeadline)) {
      bestFlygbuss =
          flygbussTrips.stream()
              .filter(flygbuss -> flygbuss.getArrival().isBefore(arrivalDeadline))
              .min(
                  Comparator.comparing(
                      flygbuss -> Duration.between(flygbuss.getArrival(), arrivalDeadline)))
              .get();
    }

    // if bus arrives too early, check the next one
    long timeDifference = Duration.between(flygbussArrival, arrivalDeadline).toMinutes();
    if (timeDifference > 30) {
      int i = flygbussTrips.indexOf(bestFlygbuss);
      Flygbuss nextFlygbuss = flygbussTrips.get(i + 1);
      LocalTime arrival = nextFlygbuss.getArrival();
      long timeDifferenceNextBus = Duration.between(arrivalDeadline, arrival).toMinutes();
      if (timeDifferenceNextBus < 10) {
        return nextFlygbuss;
      }
    }
    return bestFlygbuss;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private List<String> getBestSlTripInfo() {
    ArrayList<String> tripInfo = new ArrayList<>();
    Trip bestTrip = filterBestSlDeparture();
    this.setCurrentSlTrip(bestTrip);
    List<TripPart> tripParts = bestTrip.getTrip();
    for (TripPart tripPart : tripParts) {
      tripInfo.add(tripPart.tripAsString());
    }
    return tripInfo;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private Optional<List<String>> getPreviousSlTrip() {
    ArrayList<String> tripInfo = new ArrayList<>();
    int i = slTrips.indexOf(currentSlTrip);
    if (i > 0) {
      Trip prevTrip = slTrips.get(i - 1);
      this.setCurrentSlTrip(prevTrip);
      List<TripPart> trip = prevTrip.getTrip();
      for (TripPart tripPart : trip) {
        tripInfo.add(tripPart.tripAsString());
      }
      return Optional.of(tripInfo);
    }
    return Optional.empty();
  }

  private LocalTime getSlDepartureTime() {
    List<TripPart> trip = currentSlTrip.getTrip();
    return trip.get(0).getOriginTime();
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private Trip filterBestSlDeparture() {
    return slTrips.stream()
        .filter(trip -> !getSlArrivalTime(trip).isAfter(slDeadLine))
        .min(Comparator.comparing(trip -> Duration.between(getSlArrivalTime(trip), slDeadLine)))
        .get();
  }

  private LocalTime getSlArrivalTime(Trip trip) {
    List<TripPart> tripParts = trip.getTrip();
    // getting last TripPart because it contains arrival time
    TripPart lastTripPart = tripParts.get(tripParts.size() - 1);
    return lastTripPart.getDestinationTime();
  }

  public void setSlTrips(List<Trip> slTrips) {
    this.slTrips = slTrips;
  }

  private void setCurrentSlTrip(Trip currentSlTrip) {
    this.currentSlTrip = currentSlTrip;
  }

  private void setFlygbussDeparture(LocalTime flygbussDeparture) {
    this.flygbussDeparture = flygbussDeparture;
  }

  private void setSlDeadLine(LocalTime slDeadLine) {
    this.slDeadLine = slDeadLine;
  }

  public void setStationId(int stationId) {
    this.stationId = stationId;
  }

  public void setTimeWithBaggage(int timeWithBaggage) {
    this.timeWithBaggage = Duration.ofMinutes(timeWithBaggage);
  }

  public void setTimeNoBaggage(int timeNoBaggage) {
    this.timeNoBaggage = Duration.ofMinutes(timeNoBaggage);
  }

  public void setWalkToStation(int walkToStation) {
    this.walkToStation = Duration.ofMinutes(walkToStation);
  }

  private void setFlygbussTrips(List<Flygbuss> flygbussTrips) {
    this.flygbussTrips = flygbussTrips;
  }
}
