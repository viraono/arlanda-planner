package com.vseravno.arlandaplanner.swedavia;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vseravno.arlandaplanner.BuildConfig;
import com.vseravno.arlandaplanner.model.Flight;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiresApi(api = Build.VERSION_CODES.O)
public class SwedaviaFlightClient {
  private final ZoneId ZONE = ZoneId.of("Europe/Stockholm");
  private String url = "https://api.swedavia.se/flightinfo/v2/ARN/departures/";
  private static final String swedAviaKey = BuildConfig.swedaviaFlightInfoKey;

  public SwedaviaFlightClient() {}

  @VisibleForTesting
  public SwedaviaFlightClient(String url) {
    this.url = url;
  }

  public Flight[] getFlightsByCity(LocalDate date, String city, boolean baggage)
      throws SwedAviaException {
    JsonNode allFlights = requestAllFlights(swedAviaKey, date);
    List<JsonNode> allScheduledFlights = filterScheduledFlights(allFlights);
    return filterFlightsByCity(allScheduledFlights, city, baggage);
  }

  public Flight[] getFlightsByCallSign(LocalDate date, String callsign, boolean baggage)
      throws SwedAviaException {
    JsonNode allFlights = requestAllFlights(swedAviaKey, date);
    List<JsonNode> allScheduledFlights = filterScheduledFlights(allFlights);
    return filterFlightsByCallsign(allScheduledFlights, callsign, baggage);
  }

  private Flight[] filterFlightsByCity(
      List<JsonNode> allScheduledFlights, String city, boolean baggage) {
    String userCity = StringUtils.stripAccents(city.toLowerCase());
    ArrayList<Flight> userFlights = new ArrayList<>();
    for (JsonNode jsonFlight : allScheduledFlights) {
      String arrivalCityEn =
          StringUtils.stripAccents(jsonFlight.get("arrivalAirportEnglish").asText().toLowerCase());
      String arrivalCitySwe =
          StringUtils.stripAccents(jsonFlight.get("arrivalAirportSwedish").asText().toLowerCase());
      if (arrivalCityEn.contains(userCity) || arrivalCitySwe.contains(userCity)) {
        userFlights.add(createFlight(jsonFlight, baggage));
      }
    }
    return userFlights.toArray(new Flight[0]);
  }

  private Flight[] filterFlightsByCallsign(
      List<JsonNode> allScheduledFlights, String callSign, boolean baggage) {
    String userCallSign = StringUtils.stripAccents(callSign.toLowerCase());
    ArrayList<Flight> userFlights = new ArrayList<>();
    for (JsonNode jsonFlight : allScheduledFlights) {
      String flightCallSign =
          StringUtils.stripAccents(
              jsonFlight.get("flightLegIdentifier").get("callsign").asText().toLowerCase());
      if (flightCallSign.contains(userCallSign)) {
        userFlights.add(createFlight(jsonFlight, baggage));
      }
    }
    return userFlights.toArray(new Flight[0]);
  }

  private Flight createFlight(JsonNode flight, boolean baggage) {
    String destination = flight.get("arrivalAirportEnglish").asText();
    String callSign = flight.get("flightLegIdentifier").get("callsign").asText();
    String flightDate = flight.get("departureTime").get("scheduledUtc").asText();
    ZonedDateTime dateTime = ZonedDateTime.parse(flightDate).withZoneSameInstant(ZONE);
    LocalTime departureTime = dateTime.toLocalTime();
    LocalDate departureDate = dateTime.toLocalDate();
    return new Flight(destination, callSign, departureTime, departureDate, baggage);
  }

  private JsonNode requestAllFlights(String key, LocalDate date) throws SwedAviaException {
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();
    try {
      Request request =
          new Request.Builder()
              .header("Accept", "application/json")
              .header("Cache-Control", "no-cache")
              .header("Ocp-Apim-Subscription-Key", key)
              .url(url + date.toString())
              .build();
      try (okhttp3.Response response = client.newCall(request).execute()) {
        if (response.code() != 200) {
          throw new SwedAviaException("SwedAviaFlight returned code: " + response.code());
        }
        return mapper.readTree(response.body().string()).get("flights");
      } catch (JsonParseException e) {
        throw new SwedAviaException("Failed to parse SwedAviaFlight response", e);
      }
    } catch (IOException | NullPointerException e) {
      throw new SwedAviaException("Failed to get SwedAviaFlight response", e);
    }
  }

  private List<JsonNode> filterScheduledFlights(JsonNode flights) throws SwedAviaException {
    try {
      return StreamSupport.stream(flights.spliterator(), false)
          .filter(
              flight ->
                  flight
                      .get("locationAndStatus")
                      .get("flightLegStatusEnglish")
                      .asText()
                      .equals("Scheduled"))
          .collect(Collectors.toList());
    } catch (NullPointerException e) {
      throw new SwedAviaException("Failed to parse SwedAviaFlight json", e);
    }
  }
}
