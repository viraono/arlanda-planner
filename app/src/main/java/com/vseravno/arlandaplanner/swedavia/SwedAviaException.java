package com.vseravno.arlandaplanner.swedavia;

public class SwedAviaException extends Exception {
  public SwedAviaException(String message) {
    super(message);
  }

  public SwedAviaException(String message, Throwable cause) {
    super(message, cause);
  }
}
