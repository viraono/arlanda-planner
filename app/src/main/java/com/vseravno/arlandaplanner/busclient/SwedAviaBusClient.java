package com.vseravno.arlandaplanner.busclient;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vseravno.arlandaplanner.BuildConfig;
import com.vseravno.arlandaplanner.model.Flygbuss;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class SwedAviaBusClient {
  private static final Logger logger = LoggerFactory.getLogger(SwedAviaBusClient.class);
  private final String terminal4 = "533206";
  private final String key = BuildConfig.swedaviaArportInfoKey;
  private String url = "https://api.swedavia.se/airportinfo/v1/timetable/ARN/bus/arrivals/";

  @VisibleForTesting
  public SwedAviaBusClient(String url) {
    this.url = url;
  }

  public SwedAviaBusClient() {}

  // TODO wait for swedavia to fix their api
  @RequiresApi(api = Build.VERSION_CODES.O)
  public List<Flygbuss> getBusDepartures(LocalDate date, LocalTime arrivalDeadline)
      throws BusClientException {
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();
    Request request =
        new Request.Builder()
            .header("Cache-Control", "no-cache")
            .header("Ocp-Apim-Subscription-Key", key)
            .url(url + terminal4)
            .build();
    try (okhttp3.Response response = client.newCall(request).execute()) {
      if (response.code() != 200) {
        throw new BusClientException("SwedAvia returned code: " + response.code());
      }
      logger.info("Swedavia fixed their bus schedule api");
      return parseJson(mapper.readTree(response.body().string()));
    } catch (JsonParseException e) {
      throw new BusClientException("Failed to parse SwedAvia request", e);
    } catch (IOException | NullPointerException e) {
      throw new BusClientException("Failed to get SwedAvia response", e);
    }
  }

  // TODO wait for swedavia to fix their api
  private List<Flygbuss> parseJson(JsonNode node) {
    ArrayList<Flygbuss> busArrivals = new ArrayList<>();
    return busArrivals;
  }
}
