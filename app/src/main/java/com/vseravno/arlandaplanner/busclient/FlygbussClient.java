package com.vseravno.arlandaplanner.busclient;

import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vseravno.arlandaplanner.model.Flygbuss;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class FlygbussClient {
  private final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
  private final ZoneId ZONE = ZoneId.of("Europe/Stockholm");
  private String url = "https://www.flygbussarna.se/merresor_trafficdata/timetable/search";

  @VisibleForTesting
  public FlygbussClient(String url) {
    this.url = url;
  }

  public FlygbussClient() {}

  public List<Flygbuss> getBusDepartures(LocalDate date, LocalTime arrivalDeadline)
      throws BusClientException {
    String datetime = formatDateTime(date, arrivalDeadline);
    OkHttpClient client = new OkHttpClient();
    ObjectMapper mapper = new ObjectMapper();

    MultipartBody formBody =
        new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("form_key", "sAyNWwtpnEi5aiP8")
            .addFormDataPart("fromStopId", "1")
            .addFormDataPart("toStopId", "171")
            .addFormDataPart("time", datetime)
            .addFormDataPart("arriveAtTheLatest", "1")
            .addFormDataPart("numberOfDeparturesBeforeBestMatch", "2")
            .addFormDataPart("numberOfDeparturesAfterBestMatch", "2")
            .addFormDataPart("limit", "0")
            .build();

    Request request =
        new Request.Builder()
            .header(
                "Content-Type",
                "multipart/form-data; boundary=----WebKitFormBoundarynrCisOAO6zBhwkww")
            .addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
            .addHeader(
                "User-Agent",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15")
            .addHeader("X-Requested-With", "XMLHttpRequest")
            .post(formBody)
            .url(url)
            .build();
    try (okhttp3.Response response = client.newCall(request).execute()) {
      return getDepartures(mapper.readTree(response.body().string()).get("data").get("departures"));
    } catch (JsonParseException e) {
      throw new BusClientException("Failed to parse Flygbuss response", e);
    } catch (IOException | NullPointerException e) {
      throw new BusClientException("Failed to get FlygBuss response", e);
    }
  }

  private List<Flygbuss> getDepartures(JsonNode departures) {
    ArrayList<Flygbuss> flygbussDepartures = new ArrayList<>();
    for (JsonNode departure : departures) {
      Flygbuss flygbuss = createFlygbuss(departure);
      flygbussDepartures.add(flygbuss);
    }
    return flygbussDepartures;
  }

  private Flygbuss createFlygbuss(JsonNode node) {
    LocalDateTime departureTime =
        LocalDateTime.parse(node.get("departureTime").asText(), DATETIME_FORMAT);
    LocalDateTime arrivalTime =
        LocalDateTime.parse(node.get("arrivalTime").asText(), DATETIME_FORMAT);
    boolean match = node.get("bestMatch").asBoolean();
    return new Flygbuss(departureTime.toLocalTime(), arrivalTime.toLocalTime(), match);
  }

  private String formatDateTime(LocalDate date, LocalTime arrivalDeadline) {
    ZoneOffset offset = ZONE.getRules().getOffset(LocalDateTime.of(date, arrivalDeadline));
    String datetime =
        LocalDateTime.of(date, arrivalDeadline)
            .atOffset(offset)
            .format(DATETIME_FORMAT);
    try {
      datetime = URLEncoder.encode(datetime, "UTF-8").replace("+", "%2B");
    } catch (UnsupportedEncodingException e) {
      System.out.println(e.getMessage());
    }
    return datetime;
  }
}
