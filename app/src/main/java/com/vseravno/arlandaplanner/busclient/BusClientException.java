package com.vseravno.arlandaplanner.busclient;

public class BusClientException extends Exception {
  public BusClientException(String message) {
    super(message);
  }

  public BusClientException(String message, Throwable cause) {
    super(message, cause);
  }
}
