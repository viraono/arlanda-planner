package com.vseravno.arlandaplanner.appview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.Manager;
import com.vseravno.arlandaplanner.busclient.BusClientException;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.R;
import com.vseravno.arlandaplanner.sl.client.SlClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class SecondFragment extends Fragment {
  private static final Logger logger = LoggerFactory.getLogger(SecondFragment.class);
  private final TripAdapter tripAdapter = new TripAdapter();
  private TextView calculatingMessage;
  private TextView calculatingErrorMessage;
  private Button calculateAgainButton;
  private ProgressBar progressBarSecondFragment;
  private RecyclerView recyclerView;
  private Button oneMoreSlTripButton;
  private Button backToMainScreenButton;

  public SecondFragment() {}

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View secondFragment = inflater.inflate(R.layout.fragment_second, container, false);
    setHasOptionsMenu(true);
    setViews(secondFragment);
    return secondFragment;
  }

  public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Flight flight = SecondFragmentArgs.fromBundle(getArguments()).getFlight();

    Manager manager = new Manager(this.getContext());

    findTrip(flight, manager);

    backToMainScreenButton.setOnClickListener(
        v ->
            NavHostFragment.findNavController(SecondFragment.this)
                .navigate(R.id.action_SecondFragment_to_FirstFragment));

    oneMoreSlTripButton.setOnClickListener(
        v -> {
          oneMoreTrip(manager);
        });

    calculateAgainButton.setOnClickListener(
        v -> {
          calculatingMessage.setVisibility(View.VISIBLE);
          progressBarSecondFragment.setVisibility(View.VISIBLE);
          calculatingErrorMessage.setVisibility(View.GONE);
          calculateAgainButton.setVisibility(View.GONE);
          findTrip(flight, manager);
        });
  }

  private void findTrip(Flight flight, Manager manager) {
    String[] trip;
    try {
      trip = manager.calculateTrip(flight);
    } catch (BusClientException | SlClientException e) {
      logger.error("Failed to get trip data. ", e);
      Toast.makeText(this.getContext(), "An error occured, try again!", Toast.LENGTH_SHORT).show();
      calculatingMessage.setVisibility(View.GONE);
      progressBarSecondFragment.setVisibility(View.GONE);
      calculateAgainButton.setVisibility(View.VISIBLE);
      return;
    }
    calculatingMessage.setVisibility(View.GONE);
    progressBarSecondFragment.setVisibility(View.GONE);
    setAdapter(trip);
    oneMoreSlTripButton.setVisibility(View.VISIBLE);
  }

  private void oneMoreTrip(Manager manager) {
    String[] trip = manager.showOneMoreSlTrip();
    tripAdapter.addTrip(trip);
  }

  private void setAdapter(String[] trip) {
    recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 1));
    recyclerView.setAdapter(tripAdapter);
    tripAdapter.setTrip(trip);
    recyclerView.setVisibility(View.VISIBLE);
  }

  private void setViews(View view) {
    setCalculatingMessage(view.findViewById(R.id.calculatingMessage));
    setProgressBarSecondFragment(view.findViewById(R.id.progressBarSecondFragment));
    setOneMoreSlTripButton(view.findViewById(R.id.oneMoreSlTripButton));
    setRecyclerView(view.findViewById(R.id.tripview));
    setCalculateAgainButton(view.findViewById(R.id.calculateAgainButton));
    setCalculatingErrorMessage(view.findViewById(R.id.calculatingErrorMessage));
    setBackToMainScreenButton(view.findViewById(R.id.backToMainScreenButton));
  }

  private void setCalculatingMessage(TextView calculatingMessage) {
    this.calculatingMessage = calculatingMessage;
  }

  private void setProgressBarSecondFragment(ProgressBar progressBarSecondFragment) {
    this.progressBarSecondFragment = progressBarSecondFragment;
  }

  private void setOneMoreSlTripButton(Button oneMoreSlTripButton) {
    this.oneMoreSlTripButton = oneMoreSlTripButton;
  }

  public void setRecyclerView(RecyclerView recyclerView) {
    this.recyclerView = recyclerView;
  }

  public void setCalculatingErrorMessage(TextView calculatingErrorMessage) {
    this.calculatingErrorMessage = calculatingErrorMessage;
  }

  public void setCalculateAgainButton(Button calculateAgainButton) {
    this.calculateAgainButton = calculateAgainButton;
  }

  public void setBackToMainScreenButton(Button backToMainScreenButton) {
    this.backToMainScreenButton = backToMainScreenButton;
  }
}
