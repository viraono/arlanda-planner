package com.vseravno.arlandaplanner.appview;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.R;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

  private Flight[] flights;
  private LayoutInflater mInflater;
  private ItemClickListener mClickListener;

  // data is passed into the constructor
  FlightAdapter(Context context, Flight[] data) {
    this.mInflater = LayoutInflater.from(context);
    this.flights = data;
  }

  // inflates the cell layout from xml when needed
  @Override
  @NonNull
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = mInflater.inflate(R.layout.r_view_flight_item, parent, false);
    return new ViewHolder(view);
  }

  // binds the data to the TextView in each cell
  @Override
  @RequiresApi(api = Build.VERSION_CODES.O)
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.flightButton.setText(flights[position].generateButtonText());
  }

  // total number of cells
  @Override
  public int getItemCount() {
    return flights.length;
  }

  // stores and recycles views as they are scrolled off screen
  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView flightButton;

    ViewHolder(View itemView) {
      super(itemView);
      flightButton = itemView.findViewById(R.id.flightButton);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
    }
  }

  // convenience method for getting data at click position
  Flight getItem(int id) {
    return flights[id];
  }

  // allows clicks events to be caught
  void setClickListener(ItemClickListener itemClickListener) {
    this.mClickListener = itemClickListener;
  }

  // parent activity will implement this method to respond to click events
  public interface ItemClickListener {
    void onItemClick(View view, int position);
  }
}
