package com.vseravno.arlandaplanner.appview;

import android.os.Build;
import android.os.Bundle;
import android.view.*;

import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.R;
import com.vseravno.arlandaplanner.swedavia.SwedAviaException;
import com.vseravno.arlandaplanner.swedavia.SwedaviaFlightClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class FirstFragment extends Fragment {
  private static final Logger logger = LoggerFactory.getLogger(FirstFragment.class);
  private static final int rbIdToday = 0;
  private static final int rbIdTomorrow = 1;
  private Button findFlightButton;
  private TextView flightHintMessage;
  private TextView userStation;
  private View progressBar;
  private RecyclerView recyclerView;
  private RadioGroup radioGroup;
  private CheckBox baggageCheckBox;
  private RadioButton today;
  private RadioButton tomorrow;
  private Group flightDetails;
  private AutoCompleteTextView cityDestinationInput;
  private Group findByCallSignGroup;
  private Group findByCityGroup;
  private TextView noFlightsByCallSignMessage;
  private EditText callSignInput;
  private Button findByCallSignButton;

  public FirstFragment() {}

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (container != null) {
      container.removeAllViews();
    }
    // Inflate the layout for this fragment
    View firstFragment = inflater.inflate(R.layout.fragment_first, container, false);
    setHasOptionsMenu(true);
    setViews(firstFragment);
    userStation.setText(PreferencesManager.getStationName(firstFragment.getContext()));
    return firstFragment;
  }

  @Override
  @RequiresApi(api = Build.VERSION_CODES.O)
  public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    SwedaviaFlightClient client = new SwedaviaFlightClient();

    findFlightButton.setOnClickListener(
        v -> {
          int radioButtonId = radioGroup.getCheckedRadioButtonId();
          if (radioButtonId == -1) {
            Toast.makeText(view.getContext(), "Please select the day", Toast.LENGTH_SHORT).show();
            return;
          }
          String userCityInput = cityDestinationInput.getText().toString();
          if (userCityInput.matches("")) {
            Toast.makeText(view.getContext(), "Please enter your city", Toast.LENGTH_SHORT).show();
            return;
          }
          LocalDate date = getDate(radioButtonId);
          flightDetails.setVisibility(View.GONE);
          findByCityGroup.setVisibility(View.GONE);
          noFlightsByCallSignMessage.setVisibility(View.INVISIBLE);
          progressBar.setVisibility(View.VISIBLE);
          cityDestinationInput.getText().clear();
          try {
            findFlightsByCity(date, userCityInput, baggageCheckBox.isChecked(), client);
          } catch (SwedAviaException e) {
            logger.error("Failed to get flights by city. ", e);
            Toast.makeText(view.getContext(), "An error occured, try again!", Toast.LENGTH_SHORT)
                .show();
            flightDetails.setVisibility(View.VISIBLE);
            findByCityGroup.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
          }
        });

    findByCallSignButton.setOnClickListener(
        v -> {
          String userCallSignInput = callSignInput.getText().toString();
          if (userCallSignInput.matches("")) {
            Toast.makeText(view.getContext(), "Please enter callsign", Toast.LENGTH_SHORT).show();
            return;
          }
          LocalDate date = getDate(radioGroup.getCheckedRadioButtonId());
          flightDetails.setVisibility(View.GONE);
          findByCallSignGroup.setVisibility(View.GONE);
          progressBar.setVisibility(View.VISIBLE);
          callSignInput.getText().clear();
          try {
            findFlightsByCallSign(date, baggageCheckBox.isChecked(), userCallSignInput, client);
          } catch (SwedAviaException e) {
            logger.error("Failed to get flights by callsign. ", e);
            Toast.makeText(view.getContext(), "An error occured, try again!", Toast.LENGTH_SHORT)
                .show();
            flightDetails.setVisibility(View.VISIBLE);
            findByCallSignGroup.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
          }
        });
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private void findFlightsByCity(
      LocalDate date, String userCity, boolean baggage, SwedaviaFlightClient client)
      throws SwedAviaException {
    Flight[] flights = client.getFlightsByCity(date, userCity, baggage);
    if (flights.length == 0) {
      progressBar.setVisibility(View.GONE);
      flightDetails.setVisibility(View.VISIBLE);
      findByCallSignGroup.setVisibility(View.VISIBLE);
      progressBar.setVisibility(View.GONE);
      return;
    }
    progressBar.setVisibility(View.GONE);
    flightHintMessage.setVisibility(View.VISIBLE);
    setAdapter(flights);
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private void findFlightsByCallSign(
      LocalDate date, boolean baggage, String callSign, SwedaviaFlightClient client)
      throws SwedAviaException {
    Flight[] flights = client.getFlightsByCallSign(date, callSign, baggage);
    if (flights.length == 0) {
      progressBar.setVisibility(View.GONE);
      findByCallSignGroup.setVisibility(View.GONE);
      noFlightsByCallSignMessage.setVisibility(View.VISIBLE);
      findByCityGroup.setVisibility(View.VISIBLE);
      flightDetails.setVisibility(View.VISIBLE);
      return;
    }
    progressBar.setVisibility(View.GONE);
    flightHintMessage.setVisibility(View.VISIBLE);
    setAdapter(flights);
  }

  private void setAdapter(Flight[] flights) {
    recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
    final FlightAdapter adapter = new FlightAdapter(this.getContext(), flights);
    adapter.setClickListener(
        (view1, position) -> {
          FirstFragmentDirections.ActionFirstFragmentToSecondFragment
              actionFirstFragmentToSecondFragment =
                  FirstFragmentDirections.actionFirstFragmentToSecondFragment(
                      adapter.getItem(position));
          NavHostFragment.findNavController(FirstFragment.this)
              .navigate(actionFirstFragmentToSecondFragment);
        });
    recyclerView.setAdapter(adapter);
    recyclerView.setVisibility(View.VISIBLE);
  }

  private LocalDate getDate(int radioButtonId) {
    if (radioButtonId == today.getId()) {
      return LocalDate.now();
    } else if (radioButtonId == tomorrow.getId()) {
      return LocalDate.now().plusDays(1);
    }
    return null;
  }

  private void setViews(View firstFragment) {
    setFindFlightButton(firstFragment.findViewById(R.id.findFlightButton));
    setProgressBar(firstFragment.findViewById(R.id.progressBar));
    setFlightHintMessage(firstFragment.findViewById(R.id.flightHintMessage));
    setRecyclerView(firstFragment.findViewById(R.id.flightsView));
    setRadioGroup(firstFragment.findViewById(R.id.radioGroup));
    setUserStation(firstFragment.findViewById(R.id.userStation));
    setBaggageCheckBox(firstFragment.findViewById(R.id.baggageCheckBox));
    setToday(firstFragment.findViewById(R.id.buttonToday));
    setTomorrow(firstFragment.findViewById(R.id.buttonTomorrow));
    setFlightDetails(firstFragment.findViewById(R.id.flightDetails));
    setFindByCallSignGroup(firstFragment.findViewById(R.id.findByCallSignGroup));
    setCallSignInput(firstFragment.findViewById(R.id.callSignInput));
    setCityDestinationInput(firstFragment.findViewById(R.id.cityDestinationInput));
    setFindByCallSignButton(firstFragment.findViewById(R.id.findByCallSignButton));
    setFindByCityGroup(firstFragment.findViewById(R.id.findByCityGroup));
    setNoFlightsByCallSignMessage(firstFragment.findViewById(R.id.noFlightsByCallSignMessage));
  }

  private void setFindFlightButton(Button findFlightButton) {
    this.findFlightButton = findFlightButton;
  }

  private void setFlightHintMessage(TextView flightHintMessage) {
    this.flightHintMessage = flightHintMessage;
  }

  private void setProgressBar(View progressBar) {
    this.progressBar = progressBar;
  }

  private void setRecyclerView(RecyclerView recyclerView) {
    this.recyclerView = recyclerView;
  }

  public void setRadioGroup(RadioGroup radioGroup) {
    this.radioGroup = radioGroup;
  }

  public void setUserStation(TextView userStation) {
    this.userStation = userStation;
  }

  public void setBaggageCheckBox(CheckBox baggageCheckBox) {
    this.baggageCheckBox = baggageCheckBox;
  }

  public void setToday(RadioButton today) {
    this.today = today;
    this.today.setId(rbIdToday);
  }

  public void setTomorrow(RadioButton tomorrow) {
    this.tomorrow = tomorrow;
    this.tomorrow.setId(rbIdTomorrow);
  }

  public void setFlightDetails(Group flightDetails) {
    this.flightDetails = flightDetails;
  }

  public void setFindByCallSignGroup(Group findByCallSignGroup) {
    this.findByCallSignGroup = findByCallSignGroup;
  }

  public void setCityDestinationInput(AutoCompleteTextView cityDestinationInput) {
    this.cityDestinationInput = cityDestinationInput;
  }

  public void setCallSignInput(EditText callSignInput) {
    this.callSignInput = callSignInput;
  }

  public void setFindByCallSignButton(Button findByCallSignButton) {
    this.findByCallSignButton = findByCallSignButton;
  }

  public void setFindByCityGroup(Group findByCityGroup) {
    this.findByCityGroup = findByCityGroup;
  }

  public void setNoFlightsByCallSignMessage(TextView noFlightsByCallSignMessage) {
    this.noFlightsByCallSignMessage = noFlightsByCallSignMessage;
  }
}
