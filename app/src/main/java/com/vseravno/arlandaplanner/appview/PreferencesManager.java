package com.vseravno.arlandaplanner.appview;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {
  private static final String SHARED_PREFERENCES = "com.vseravno.arlandaplanner";
  private static final String NO_BAGGAGE_TIME = "noBaggage";
  private static final String WITH_BAGGAGE_TIME = "withBaggage";
  private static final String STATION_NAME = "stationName";
  private static final String STATION_ID = "stationId";
  private static final String WALK_TO_STATION = "walkingTime";

  public static boolean arePreferencesFilled(Context context) {
    return getStationName(context) != null && getWalkToStation(context) != 0;
  }

  private static SharedPreferences getSharedPreferences(Context context) {
    return context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
  }

  public static int getNoBaggageTime(Context context) {
    return getSharedPreferences(context).getInt(NO_BAGGAGE_TIME, 0);
  }

  public static int getWithBaggageTime(Context context) {
    return getSharedPreferences(context).getInt(WITH_BAGGAGE_TIME, 0);
  }

  public static String getStationName(Context context) {
    return getSharedPreferences(context).getString(STATION_NAME, null);
  }

  public static int getWalkToStation(Context context) {
    return getSharedPreferences(context).getInt(WALK_TO_STATION, 0);
  }

  public static int getStationId(Context context) {
    return getSharedPreferences(context).getInt(STATION_ID, 0);
  }

  public static void setStationId(Context context, int stationId) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.putInt(STATION_ID, stationId);
    editor.apply();
  }

  public static void setNoBaggageTime(Context context, int time) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.putInt(NO_BAGGAGE_TIME, time);
    editor.apply();
  }

  public static void setWithBaggageTime(Context context, int time) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.putInt(WITH_BAGGAGE_TIME, time);
    editor.apply();
  }

  public static void setStationName(Context context, String station) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.putString(STATION_NAME, station);
    editor.apply();
  }

  public static void setWalkToStation(Context context, int time) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.putInt(WALK_TO_STATION, time);
    editor.apply();
  }

  public static void clearPreferences(Context context) {
    final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
    editor.clear();
    editor.apply();
  }
}
