package com.vseravno.arlandaplanner.appview;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.Manager;
import com.vseravno.arlandaplanner.R;
import com.vseravno.arlandaplanner.model.Station;
import com.vseravno.arlandaplanner.sl.client.SlClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PreferencesFragment extends Fragment {
  private static final Logger logger = LoggerFactory.getLogger(PreferencesFragment.class);
  private EditText prefWithBaggageTimeInput;
  private EditText prefNoBaggageTimeInput;
  private EditText prefStationInput;
  private EditText prefWalkingInput;
  private TextView prefStationName;
  private Button prefFindStationButton;
  private Button prefChangeStationButton;
  private ProgressBar prefProgressBarStationSearch;
  private RecyclerView prefStationsView;
  private Group prefInitialStation;
  private Group prefChosenStation;
  private Group prefFlight;
  private Button prefSaveButton;
  private TextView prefInitialInfo;
  private TextView prefNoStationFoundText;
  private TextView prefStationText;
  private TextView prefChooseStationText;
  private TextView prefStationTitle;

  public PreferencesFragment() {}

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View preferences = inflater.inflate(R.layout.preferences, container, false);
    setViews(preferences);
    if (PreferencesManager.arePreferencesFilled(getContext())) {
      prefNoBaggageTimeInput.setText(
          String.valueOf(PreferencesManager.getNoBaggageTime(getContext())));
      prefWithBaggageTimeInput.setText(
          String.valueOf(PreferencesManager.getWithBaggageTime(getContext())));
      prefWalkingInput.setText(String.valueOf(PreferencesManager.getWalkToStation(getContext())));
      prefStationName.setText(PreferencesManager.getStationName(getContext()));
    }
    return preferences;
  }

  @Override
  public void onViewCreated(@NonNull final View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    if (PreferencesManager.arePreferencesFilled(this.getContext())) {
      prefInitialStation.setVisibility(View.GONE);
      prefChosenStation.setVisibility(View.VISIBLE);
      prefInitialInfo.setVisibility(View.GONE);
    }

    Manager manager = new Manager();

    prefFindStationButton.setOnClickListener(
        v -> {
          // get user input
          String stationName = prefStationInput.getText().toString();
          if (stationName.matches("")) {
            Toast.makeText(view.getContext(), "Please enter your station", Toast.LENGTH_SHORT)
                .show();
            return;
          }
          try {
            showStations(stationName, manager);
          } catch (SlClientException e) {
            logger.error("Failed to get stations from SL. ", e);
            Toast.makeText(view.getContext(), "An error occured, try again!", Toast.LENGTH_SHORT)
                .show();
            prefInitialStation.setVisibility(View.VISIBLE);
            prefProgressBarStationSearch.setVisibility(View.GONE);
          }
        });

    prefChangeStationButton.setOnClickListener(
        v -> {
          prefChosenStation.setVisibility(View.GONE);
          prefInitialStation.setVisibility(View.VISIBLE);
        });

    prefSaveButton.setOnClickListener(
        v -> {
          String noBaggageTime = prefNoBaggageTimeInput.getText().toString();
          String withBaggageTime = prefWithBaggageTimeInput.getText().toString();
          String walkingTime = prefWalkingInput.getText().toString();
          if (noBaggageTime.matches("") || withBaggageTime.matches("") || walkingTime.matches("")) {
            Toast.makeText(
                    view.getContext(), "Please enter all the needed times", Toast.LENGTH_SHORT)
                .show();
            return;
          }
          PreferencesManager.setNoBaggageTime(view.getContext(), Integer.parseInt(noBaggageTime));
          PreferencesManager.setWithBaggageTime(
              view.getContext(), Integer.parseInt(withBaggageTime));
          PreferencesManager.setWalkToStation(view.getContext(), Integer.parseInt(walkingTime));
          NavHostFragment.findNavController(this).navigate(R.id.FirstFragment);
        });
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private void showStations(String stationName, Manager manager) throws SlClientException {
    prefInitialStation.setVisibility(View.GONE);
    prefNoStationFoundText.setVisibility(View.GONE);
    prefProgressBarStationSearch.setVisibility(View.VISIBLE);
    prefStationInput.getText().clear();
    Station[] stations = manager.getStations(stationName);
    if (stations.length == 0) {
      prefProgressBarStationSearch.setVisibility(View.GONE);
      prefStationText.setVisibility(View.GONE);
      prefNoStationFoundText.setVisibility(View.VISIBLE);
      prefStationInput.setVisibility(View.VISIBLE);
      prefFindStationButton.setVisibility(View.VISIBLE);
      return;
    }
    prefProgressBarStationSearch.setVisibility(View.GONE);
    setAdapter(stations);
  }

  private void setChosenStation(Station station) {
    prefStationsView.setVisibility(View.GONE);
    prefChooseStationText.setVisibility(View.GONE);
    prefFlight.setVisibility(View.VISIBLE);
    prefStationName.setText(station.getStationName());
    prefChosenStation.setVisibility(View.VISIBLE);
    prefStationTitle.setVisibility(View.VISIBLE);
    PreferencesManager.setStationName(this.getContext(), station.getStationName());
    PreferencesManager.setStationId(this.getContext(), station.getStationId());
  }

  private void setAdapter(Station[] stations) {
    prefStationsView.setLayoutManager(new GridLayoutManager(this.getContext(), 1));
    final StationAdapter adapter = new StationAdapter(this.getContext(), stations);
    prefStationsView.setAdapter(adapter);
    adapter.setClickListener(
        (view1, position) -> {
          Station chosenStation = adapter.getItem(position);
          setChosenStation(chosenStation);
        });
    prefChooseStationText.setVisibility(View.VISIBLE);
    prefFlight.setVisibility(View.GONE);
    prefStationTitle.setVisibility(View.GONE);
    prefStationsView.setVisibility(View.VISIBLE);
  }

  private void setViews(View preferences) {
    setPrefChangeStationButton(preferences.findViewById(R.id.prefChangeStationButton));
    setPrefChosenStation(preferences.findViewById(R.id.prefChosenStation));
    setPrefFindStationButton(preferences.findViewById(R.id.prefFindStationButton));
    setPrefInitialStation(preferences.findViewById(R.id.prefInitialStation));
    setPrefNoBaggageTimeInput(preferences.findViewById(R.id.prefNoBaggageTimeInput));
    setPrefProgressBarStationSearch(preferences.findViewById(R.id.prefProgressBarStationSearch));
    setPrefWithBaggageTimeInput(preferences.findViewById(R.id.prefWithBaggageTimeInput));
    setPrefStationInput(preferences.findViewById(R.id.prefStationInput));
    setPrefWalkingInput(preferences.findViewById(R.id.prefWalkingInput));
    setPrefStationsView(preferences.findViewById(R.id.prefStationsView));
    setPrefSaveButton(preferences.findViewById(R.id.prefSaveButton));
    setPrefStationName(preferences.findViewById(R.id.prefStationName));
    setPrefInitialInfo(preferences.findViewById(R.id.prefInitialInfo));
    setPrefNoStationFoundText(preferences.findViewById(R.id.prefNoStationFoundText));
    setPrefStationText(preferences.findViewById(R.id.prefStationText));
    setPrefChooseStationText(preferences.findViewById(R.id.prefChooseStationText));
    setPrefFlight(preferences.findViewById(R.id.prefFlight));
    setPrefStationTitle(preferences.findViewById(R.id.prefStationTitle));
  }

  public void setPrefWithBaggageTimeInput(EditText prefWithBaggageTimeInput) {
    this.prefWithBaggageTimeInput = prefWithBaggageTimeInput;
  }

  public void setPrefNoBaggageTimeInput(EditText prefNoBaggageTimeInput) {
    this.prefNoBaggageTimeInput = prefNoBaggageTimeInput;
  }

  public void setPrefStationInput(EditText prefStationInput) {
    this.prefStationInput = prefStationInput;
  }

  public void setPrefWalkingInput(EditText prefWalkingInput) {
    this.prefWalkingInput = prefWalkingInput;
  }

  public void setPrefFindStationButton(Button prefFindStationButton) {
    this.prefFindStationButton = prefFindStationButton;
  }

  public void setPrefChangeStationButton(Button prefChangeStationButton) {
    this.prefChangeStationButton = prefChangeStationButton;
  }

  public void setPrefProgressBarStationSearch(ProgressBar prefProgressBarStationSearch) {
    this.prefProgressBarStationSearch = prefProgressBarStationSearch;
  }

  public void setPrefStationsView(RecyclerView prefStationsView) {
    this.prefStationsView = prefStationsView;
  }

  public void setPrefInitialStation(Group prefInitialStation) {
    this.prefInitialStation = prefInitialStation;
  }

  public void setPrefChosenStation(Group prefChosenStation) {
    this.prefChosenStation = prefChosenStation;
  }

  public void setPrefSaveButton(Button prefSaveButton) {
    this.prefSaveButton = prefSaveButton;
  }

  public void setPrefStationName(TextView prefStationName) {
    this.prefStationName = prefStationName;
  }

  public void setPrefInitialInfo(TextView prefInitialInfo) {
    this.prefInitialInfo = prefInitialInfo;
  }

  public void setPrefNoStationFoundText(TextView prefNoStationFoundText) {
    this.prefNoStationFoundText = prefNoStationFoundText;
  }

  public void setPrefStationText(TextView prefStationText) {
    this.prefStationText = prefStationText;
  }

  public void setPrefChooseStationText(TextView prefChooseStationText) {
    this.prefChooseStationText = prefChooseStationText;
  }

  public void setPrefFlight(Group prefFlight) {
    this.prefFlight = prefFlight;
  }

  public void setPrefStationTitle(TextView prefStationTitle) {
    this.prefStationTitle = prefStationTitle;
  }
}
