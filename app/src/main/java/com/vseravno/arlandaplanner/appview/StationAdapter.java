package com.vseravno.arlandaplanner.appview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.R;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.model.Station;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.ViewHolder> {

  private Station[] stations;
  private LayoutInflater mInflater;
  private StationAdapter.ItemClickListener mClickListener;

  // data is passed into the constructor
  StationAdapter(Context context, Station[] data) {
    this.mInflater = LayoutInflater.from(context);
    this.stations = data;
  }

  // inflates the cell layout from xml when needed
  @Override
  @NonNull
  public StationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = mInflater.inflate(R.layout.r_view_station_item, parent, false);
    return new StationAdapter.ViewHolder(view);
  }

  // binds the data to the TextView in each cell
  @Override
  public void onBindViewHolder(@NonNull StationAdapter.ViewHolder holder, int position) {
    holder.stationName.setText(stations[position].getStationName());
  }

  // total number of cells
  @Override
  public int getItemCount() {
    return stations.length;
  }

  // stores and recycles views as they are scrolled off screen
  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView stationName;

    ViewHolder(View itemView) {
      super(itemView);
      itemView.setOnClickListener(this);
      stationName = itemView.findViewById(R.id.prefChooseStationItem);
    }

    @Override
    public void onClick(View view) {
      if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
    }
  }

  // convenience method for getting data at click position
  Station getItem(int id) {
    return stations[id];
  }

  // allows clicks events to be caught
  void setClickListener(StationAdapter.ItemClickListener itemClickListener) {
    this.mClickListener = itemClickListener;
  }

  // parent activity will implement this method to respond to click events
  public interface ItemClickListener {
    void onItemClick(View view, int position);
  }
}
