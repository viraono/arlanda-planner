package com.vseravno.arlandaplanner.appview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.vseravno.arlandaplanner.R;

import java.util.Arrays;
import java.util.stream.Stream;

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ViewHolder> {

  private String[] trip;

  public TripAdapter() {}

  public void setTrip(String[] trip) {
    this.trip = trip;
  }

  public TripAdapter(String[] trip) {
    this.trip = trip;
  }

  public void addTrip(String[] trip) {
    int previousLength = this.trip.length;
    String[] allTheTrips =
        Stream.concat(Arrays.stream(this.trip), Arrays.stream(trip)).toArray(String[]::new);
    setTrip(allTheTrips);
    notifyItemRangeInserted(previousLength, trip.length);
  }

  // Create new views (invoked by the layout manager)
  @Override
  public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    // Create a new view, which defines the UI of the list item
    View view =
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.r_view_trip_item, viewGroup, false);

    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder viewHolder, final int position) {
    viewHolder.getTrip().setText(trip[position]);
  }

  @Override
  public int getItemCount() {
    return trip.length;
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView trip;

    public ViewHolder(View view) {
      super(view);
      trip = (TextView) view.findViewById(R.id.tripItem);
    }

    public TextView getTrip() {
      return trip;
    }
  }
}
