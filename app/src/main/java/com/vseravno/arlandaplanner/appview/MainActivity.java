package com.vseravno.arlandaplanner.appview;

import android.os.Bundle;
import android.os.StrictMode;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import com.vseravno.arlandaplanner.R;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);

    setContentView(R.layout.activity_main);

    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    NavGraph graph = navController.getNavInflater().inflate(R.navigation.nav_graph);
    if (!PreferencesManager.arePreferencesFilled(this)) {
      graph.setStartDestination(R.id.shared_preferences);
    } else {
      graph.setStartDestination(R.id.FirstFragment);
    }
    navController.setGraph(graph);

    findViewById(R.id.settingsButton)
        .setOnClickListener(
            v -> {
              navController.navigate(R.id.shared_preferences);
            });
  }
}
