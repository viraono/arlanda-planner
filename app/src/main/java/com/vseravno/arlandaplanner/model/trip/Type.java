package com.vseravno.arlandaplanner.model.trip;

public enum Type {
  WALK,
  RIDE
}
