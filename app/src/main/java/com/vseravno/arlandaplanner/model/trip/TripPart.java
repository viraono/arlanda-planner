package com.vseravno.arlandaplanner.model.trip;

import java.time.LocalTime;

public class TripPart {
  private String origin;
  private LocalTime originTime;
  private String destination;
  private LocalTime destinationTime;
  private String name;
  private Transport transportType;
  private Type tripType;
  private int walkingDistance;
  private String direction;

  // for walk trips
  public TripPart(
      String origin,
      LocalTime originTime,
      String destination,
      LocalTime destinationTime,
      Type tripType,
      int walkingDistance) {
    this.origin = origin;
    this.originTime = originTime;
    this.destination = destination;
    this.destinationTime = destinationTime;
    this.tripType = tripType;
    this.walkingDistance = walkingDistance;
  }

  public TripPart(
      String origin,
      LocalTime originTime,
      String destination,
      LocalTime destinationTime,
      String name,
      Transport transportType,
      Type tripType,
      String direction) {
    this.origin = origin;
    this.originTime = originTime;
    this.destination = destination;
    this.destinationTime = destinationTime;
    this.name = name;
    this.transportType = transportType;
    this.tripType = tripType;
    this.direction = direction;
  }

  public String tripAsString() {
    String origin = this.getOrigin();
    LocalTime originTime = this.getOriginTime();
    String destination = this.getDestination();
    LocalTime destinationTime = this.getDestinationTime();
    if (this.getTripType().equals(Type.WALK)) {
      return "Go " + this.getWalkingDistance() + " meters to " + destination + " station. \n";
    }
    Transport transportType = this.getTransportType();
    String name = this.getName();
    String direction = this.getDirection();
    switch (transportType) {
      case BUS:
        return name
            + " (direction "
            + direction
            + ") leaves from "
            + origin
            + " at "
            + originTime
            + " and arrives to "
            + destination
            + " at "
            + destinationTime
            + ". \n";
      case TRAIN:
        return name
            + " (direction "
            + direction
            + ") leaves from "
            + origin
            + " at "
            + originTime
            + " and arrives to "
            + destination
            + " at "
            + destinationTime
            + ". \n";
      case TRAM:
        return "Tram (direction "
            + direction
            + ") leaves from "
            + origin
            + " at "
            + originTime
            + " and arrives to "
            + destination
            + " at "
            + destinationTime
            + ". \n";
      case SUBWAY:
        return "Tunnelbana (mot "
            + direction
            + ") leaves from "
            + origin
            + " at "
            + originTime
            + " and arrives to "
            + destination
            + " at "
            + destinationTime
            + ". \n";
    }
    return "";
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public LocalTime getOriginTime() {
    return originTime;
  }

  public void setOriginTime(LocalTime originTime) {
    this.originTime = originTime;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public LocalTime getDestinationTime() {
    return destinationTime;
  }

  public void setDestinationTime(LocalTime destinationTime) {
    this.destinationTime = destinationTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Type getTripType() {
    return tripType;
  }

  public void setTripType(Type tripType) {
    this.tripType = tripType;
  }

  public Transport getTransportType() {
    return transportType;
  }

  public void setTransportType(Transport transportType) {
    this.transportType = transportType;
  }

  public int getWalkingDistance() {
    return walkingDistance;
  }

  public void setWalkingDistance(int walkingDistance) {
    this.walkingDistance = walkingDistance;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }
}
