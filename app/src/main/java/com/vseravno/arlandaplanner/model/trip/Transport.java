package com.vseravno.arlandaplanner.model.trip;

public enum Transport {
  BUS,
  TRAM,
  TRAIN,
  SUBWAY
}
