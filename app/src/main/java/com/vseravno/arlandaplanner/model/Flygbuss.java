package com.vseravno.arlandaplanner.model;

import java.time.LocalTime;

public class Flygbuss {
  private LocalTime departure;
  private LocalTime arrival;
  boolean bestMatch;

  public Flygbuss(LocalTime departure, LocalTime arrival, boolean bestMatch) {
    this.departure = departure;
    this.arrival = arrival;
    this.bestMatch = bestMatch;
  }

  public LocalTime getDeparture() {
    return departure;
  }

  public void setDeparture(LocalTime departure) {
    this.departure = departure;
  }

  public LocalTime getArrival() {
    return arrival;
  }

  public void setArrival(LocalTime arrival) {
    this.arrival = arrival;
  }

  public boolean isBestMatch() {
    return bestMatch;
  }

  public void setBestMatch(boolean bestMatch) {
    this.bestMatch = bestMatch;
  }
}
