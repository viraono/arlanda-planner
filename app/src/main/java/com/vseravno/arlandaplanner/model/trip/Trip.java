package com.vseravno.arlandaplanner.model.trip;

import java.util.List;

public class Trip {
  private List<TripPart> trip;

  public Trip() {}

  public List<TripPart> getTrip() {
    return trip;
  }

  public void setTrip(List<TripPart> trip) {
    this.trip = trip;
  }
}
