package com.vseravno.arlandaplanner.model;

public class Station {
  private String stationName;
  private int stationId;

  public Station(String stationName, int stationId) {
    this.stationName = stationName;
    this.stationId = stationId;
  }

  public String getStationName() {
    return stationName;
  }

  public void setStationName(String stationName) {
    this.stationName = stationName;
  }

  public int getStationId() {
    return stationId;
  }

  public void setStationId(int stationId) {
    this.stationId = stationId;
  }
}
