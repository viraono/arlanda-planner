package com.vseravno.arlandaplanner.model;

import android.os.Build;
import androidx.annotation.RequiresApi;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

@RequiresApi(api = Build.VERSION_CODES.O)
public class Flight implements Serializable {

  private String destination;
  private String callSign;
  private LocalTime departureTime;
  private LocalDate departureDate;
  private boolean withBaggage;

  public Flight(
      String destination,
      String callSign,
      LocalTime departureTime,
      LocalDate departureDate,
      boolean withBaggage) {
    this.destination = destination;
    this.callSign = callSign;
    this.departureTime = departureTime;
    this.departureDate = departureDate;
    this.withBaggage = withBaggage;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getCallSign() {
    return callSign;
  }

  public void setCallSign(String callSign) {
    this.callSign = callSign;
  }

  public LocalTime getDepartureTime() {
    return departureTime;
  }

  public void setDepartureTime(LocalTime departureTime) {
    this.departureTime = departureTime;
  }

  public LocalDate getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(LocalDate departureDate) {
    this.departureDate = departureDate;
  }

  public boolean isWithBaggage() {
    return withBaggage;
  }

  public void setWithBaggage(boolean withBaggage) {
    this.withBaggage = withBaggage;
  }

  public String generateButtonText() {
    return "Flight to: "
        + "\n"
        + this.getDestination()
        + "\n CallSign: "
        + this.getCallSign()
        + "\n Departs at: "
        + this.getDepartureTime()
        + "\n On: "
        + this.getDepartureDate();
  }

  public static Flight[] getflights() {
    ArrayList<Flight> flights = new ArrayList<>();
    Flight[] arrayFlights = new Flight[flights.size()];
    flights.toArray(arrayFlights);
    return arrayFlights;
  }
}
