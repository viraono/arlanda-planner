import com.google.common.io.Resources;
import com.vseravno.arlandaplanner.Manager;
import com.vseravno.arlandaplanner.busclient.BusClient;
import com.vseravno.arlandaplanner.busclient.BusClientException;
import com.vseravno.arlandaplanner.model.Flight;
import com.vseravno.arlandaplanner.model.Flygbuss;
import com.vseravno.arlandaplanner.model.trip.Transport;
import com.vseravno.arlandaplanner.model.trip.Trip;
import com.vseravno.arlandaplanner.model.trip.TripPart;
import com.vseravno.arlandaplanner.model.trip.Type;
import com.vseravno.arlandaplanner.sl.client.SlClient;
import com.vseravno.arlandaplanner.sl.client.SlClientException;
import com.vseravno.arlandaplanner.sl.client.SlClientTest;
import mockwebserver3.MockWebServer;
import okhttp3.HttpUrl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.Duration.ofMinutes;

@ExtendWith(MockitoExtension.class)
public class ManagerTest {
  @Mock SlClient slClient;
  @Mock BusClient busClient;
  Manager manager;
  Flight flight;
  LocalDate date = LocalDate.of(2021, 4, 27);
  ArrayList<Flygbuss> flygbusses = new ArrayList<>();
  ArrayList<Trip> trips = new ArrayList<>();

  @BeforeEach
  void beforeEach() {
    this.manager = new Manager(slClient, busClient);
    manager.setStationId(1500);
    manager.setTimeNoBaggage(70);
    manager.setTimeWithBaggage(100);
    manager.setWalkToStation(10);
    this.flight = new Flight("Destination", "Callsign", LocalTime.of(14, 0), date, false);
    flygbusses.add(new Flygbuss(LocalTime.of(12, 0), LocalTime.of(12, 45), true));
    addTrips();
  }

  @Test
  public void calculateTripTest() throws BusClientException, SlClientException {
    String[] expectedTrip = new String[8];
    expectedTrip[0] =
        "Buss 160 (direction Årstaberg) leaves from Årsta at 11:20 and arrives to Årstaberg (terminalen) at 11:27. \n";
    expectedTrip[1] = "Go 54 meters to Årstaberg station station. \n";
    expectedTrip[2] =
        "Pendeltåg 43 (direction Bålsta) leaves from Årstaberg at 11:33 and arrives to Stockholm City at 11:39. \n";
    expectedTrip[3] = "";
    expectedTrip[4] = "Flygbuss leaves at 12:00.";
    expectedTrip[5] = "";
    expectedTrip[6] = "You need to leave at 11:10.";
    expectedTrip[7] = "____________________";

    LocalTime busArrivalDeadline = LocalTime.of(12, 50);
    LocalTime slArrivalDeadline = LocalTime.of(11, 45);

    Mockito.when(busClient.getBusTrips(date, busArrivalDeadline)).thenReturn(flygbusses);
    Mockito.when(slClient.getAllDepartures(slArrivalDeadline, date, 1500)).thenReturn(trips);
    String[] trip = manager.calculateTrip(flight);
    Assertions.assertArrayEquals(expectedTrip, trip);
  }

  @Test
  public void showOneMoreTripTest() throws BusClientException, SlClientException {

    Mockito.when(busClient.getBusTrips(Mockito.any(), Mockito.any())).thenReturn(flygbusses);
    Mockito.when(slClient.getAllDepartures(Mockito.any(), Mockito.any(), Mockito.anyInt()))
        .thenReturn(trips);
    manager.calculateTrip(flight);

    String[] expectedTrip = new String[7];
    expectedTrip[0] =
        "Buss 160 (direction Gullmarsplan) leaves from Årsta at 11:13 and arrives to Gullmarsplan at 11:22. \n";
    expectedTrip[1] =
        "Tunnelbana (mot Hässelby strand) leaves from Gullmarsplan at 11:27 and arrives to T-Centralen at 11:36. \n";
    expectedTrip[2] = "";
    expectedTrip[3] = "Flygbuss leaves at 12:00.";
    expectedTrip[4] = "";
    expectedTrip[5] = "You need to leave at 11:03.";
    expectedTrip[6] = "____________________";

    String[] trip = manager.showOneMoreSlTrip();
    Assertions.assertArrayEquals(expectedTrip, trip);

    String[] trip2 = manager.showOneMoreSlTrip();
    Assertions.assertArrayEquals(new String[] {"No more options!"}, trip2);
  }

  private void addTrips() {
    Trip trip1 = new Trip();
    ArrayList<TripPart> trip1Parts = new ArrayList<>();
    TripPart trip1Part1 =
        new TripPart(
            "Årsta",
            LocalTime.of(11, 13),
            "Gullmarsplan",
            LocalTime.of(11, 22),
            "Buss 160",
            Transport.BUS,
            Type.RIDE,
            "Gullmarsplan");
    TripPart trip1Part2 =
        new TripPart(
            "Gullmarsplan",
            LocalTime.of(11, 27),
            "T-Centralen",
            LocalTime.of(11, 36),
            "Tunnelbana",
            Transport.SUBWAY,
            Type.RIDE,
            "Hässelby strand");
    trip1Parts.add(trip1Part1);
    trip1Parts.add(trip1Part2);
    trip1.setTrip(trip1Parts);
    trips.add(trip1);

    Trip trip2 = new Trip();
    ArrayList<TripPart> trip2Parts = new ArrayList<>();
    TripPart trip2Part1 =
        new TripPart(
            "Årsta",
            LocalTime.of(11, 20),
            "Årstaberg (terminalen)",
            LocalTime.of(11, 27),
            "Buss 160",
            Transport.BUS,
            Type.RIDE,
            "Årstaberg");
    TripPart trip2Part2 =
        new TripPart(
            "Årstaberg (terminalen)",
            LocalTime.of(11, 27),
            "Årstaberg station",
            LocalTime.of(11, 30),
            Type.WALK,
            54);
    TripPart trip2Part3 =
        new TripPart(
            "Årstaberg",
            LocalTime.of(11, 33),
            "Stockholm City",
            LocalTime.of(11, 39),
            "Pendeltåg 43",
            Transport.TRAIN,
            Type.RIDE,
            "Bålsta");
    trip2Parts.add(trip2Part1);
    trip2Parts.add(trip2Part2);
    trip2Parts.add(trip2Part3);
    trip2.setTrip(trip2Parts);
    trips.add(trip2);
  }
}
