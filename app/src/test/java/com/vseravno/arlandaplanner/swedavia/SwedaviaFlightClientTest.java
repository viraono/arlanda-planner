package com.vseravno.arlandaplanner.swedavia;

import com.google.common.io.Resources;
import com.vseravno.arlandaplanner.model.Flight;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.HttpUrl;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

public class SwedaviaFlightClientTest {
  SwedaviaFlightClient client;
  MockWebServer server;
  String url = "/";
  LocalDate date = LocalDate.of(2021, 2, 3);

  private String getJson(String resourceName) {
    try {
      return Resources.toString(
          Resources.getResource(resourceName + ".json"), StandardCharsets.UTF_8);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  @BeforeEach
  void initialize() throws IOException {
    this.server = new MockWebServer();
    server.start();
    HttpUrl baseUrl = server.url(url);
    this.client = new SwedaviaFlightClient(baseUrl.toString());
  }

  @AfterEach
  void tearDown() throws IOException {
    server.shutdown();
  }

  @Test
  public void getFlightsExceptionNoResponceTest() {
    Exception exception =
        Assertions.assertThrows(
            SwedAviaException.class, () -> client.getFlightsByCity(date, "Amsterdam", true));
    String expectedMessage = "Failed to get SwedAviaFlight response";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getFlightsExceptionCodeTest() {
    server.enqueue(new MockResponse().setResponseCode(404));
    Exception exception =
        Assertions.assertThrows(
            SwedAviaException.class, () -> client.getFlightsByCity(date, "Amsterdam", true));
    String expectedMessage = "SwedAviaFlight returned code: ";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getFlightsExceptionBodyTest() {
    server.enqueue(new MockResponse().setBody("<xml>xml</xml>"));
    Exception exception =
        Assertions.assertThrows(
            SwedAviaException.class, () -> client.getFlightsByCity(date, "Amsterdam", true));
    String expectedMessage = "Failed to parse SwedAviaFlight response";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getFlightsExceptionParseTest() {
    server.enqueue(new MockResponse());
    Exception exception =
        Assertions.assertThrows(
            SwedAviaException.class, () -> client.getFlightsByCity(date, "Amsterdam", true));
    String expectedMessage = "Failed to parse SwedAviaFlight json";
    String actualMessage = exception.getMessage();
    Assertions.assertTrue(actualMessage.contains(expectedMessage));
  }

  @Test
  public void getFlightsByCityTest() throws SwedAviaException {
    server.enqueue(new MockResponse().setBody(getJson("flights")));
    server.enqueue(new MockResponse().setBody(getJson("flights")));
    server.enqueue(new MockResponse().setBody(getJson("flights")));

    Flight[] flights1 = client.getFlightsByCity(date, "Amsterdam", true);
    Assertions.assertEquals(3, flights1.length);
    Assertions.assertEquals(flights1[0].getDestination(), "Amsterdam");

    Flight[] flights2 = client.getFlightsByCity(date, "Copenhagen", true);
    Assertions.assertEquals(2, flights2.length);
    Assertions.assertEquals(flights2[0].getDestination(), "Copenhagen");

    Flight[] flights3 = client.getFlightsByCity(date, "Riga", true);
    System.out.println(flights3.length);
    Assertions.assertEquals(0, flights3.length);
  }

  @Test
  public void getFlightsByCallsignTest() throws SwedAviaException {
    server.enqueue(new MockResponse().setBody(getJson("flights")));
    server.enqueue(new MockResponse().setBody(getJson("flights")));

    Flight[] flights1 = client.getFlightsByCallSign(date, "SAS1042", true);
    Assertions.assertEquals(1, flights1.length);
    Assertions.assertEquals(flights1[0].getCallSign(), "SAS1042");

    Flight[] flights2 = client.getFlightsByCallSign(date, "VKG622", true);
    Assertions.assertEquals(0, flights2.length);
  }
}
