package com.vseravno.arlandaplanner.busclient;

import com.google.common.io.Resources;
import com.vseravno.arlandaplanner.model.Flygbuss;
import com.vseravno.arlandaplanner.sl.client.SlClient;
import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.HttpUrl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class FlygBussClientTest {
  FlygbussClient client;
  MockWebServer server;
  String url = "/";
  LocalTime deadline = LocalTime.of(12, 50);
  LocalDate date = LocalDate.of(2021, 4, 27);

  private String getJson(String resourceName) {
    try {
      return Resources.toString(
          Resources.getResource(resourceName + ".json"), StandardCharsets.UTF_8);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  @BeforeEach
  void initialize() throws IOException {
    this.server = new MockWebServer();
    server.start();
    HttpUrl baseUrl = server.url(url);
    this.client = new FlygbussClient(baseUrl.toString());
  }

  @AfterEach
  void tearDown() throws IOException {
    server.shutdown();
  }

  @Test
  void getBusDeparturesTest() throws BusClientException {
    server.enqueue(new MockResponse().setBody(getJson("flygbuss")));
    List<Flygbuss> busDepartures = client.getBusDepartures(date, deadline);
    Assertions.assertEquals(5, busDepartures.size());
    Assertions.assertEquals(true, busDepartures.get(2).isBestMatch());
  }

}
